/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import Constants from './Constants'
import FseCompressionTable from './FseCompressionTable'

export default class SequenceEncodingContext {
    private static MAX_SEQUENCES: number = Math.max(Constants.MAX_LITERALS_LENGTH_SYMBOL, Constants.MAX_MATCH_LENGTH_SYMBOL);
    public literalLengthTable: FseCompressionTable = new FseCompressionTable(Constants.LITERAL_LENGTH_TABLE_LOG, Constants.MAX_LITERALS_LENGTH_SYMBOL);
    public offsetCodeTable: FseCompressionTable = new FseCompressionTable(Constants.OFFSET_TABLE_LOG, Constants.MAX_OFFSET_CODE_SYMBOL);
    public matchLengthTable: FseCompressionTable = new FseCompressionTable(Constants.MATCH_LENGTH_TABLE_LOG, Constants.MAX_MATCH_LENGTH_SYMBOL);
    public counts: Int32Array= new Int32Array(SequenceEncodingContext.MAX_SEQUENCES + 1);
    public normalizedCounts: Int16Array = new Int16Array(SequenceEncodingContext.MAX_SEQUENCES + 1);
}
