/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import fileio from '@ohos.fileio';
import featureAbility from '@ohos.ability.featureAbility';
import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from "hypium/index";
import { InputStream, OutputStream, System, IOUtils, CompressorOutputStream, CompressorInputStream,
  CompressorStreamFactory, Data, BlockSort } from '@ohos/commons-compress';

export default function bzip2CompressorTest() {
  describe('bzip2CompressorTest', function () {

    it('testBzipCreation', 0, function (done) {
      var context = featureAbility.getContext();
      context.getFilesDir()
        .then((data) => {
          const writer = fileio.openSync(data + '/cache_images/hello.txt', 0o102, 0o666);
          fileio.writeSync(writer, "111111111111111111111111111000101011\n"
          + "111111111111111111111111111000101011\n"
          + "111111111111111111111111111000101011\n"
          + "111111111111111111111111111000101011\n"
          + "111111111111111111111111111000101011\n"
          + "111111111111111111111111111000101011\n"
          + "111111111111111111111111111000101011\n"
          + "111111111111111111111111111000101011\n"
          + "111111111111111111111111111000101011\n"
          + "111111111111111111111111111000101011"
          );
          fileio.closeSync(writer);
          let inputStream: InputStream = new InputStream();
          inputStream.setFilePath(data + '/cache_images/hello.txt');
          let fOut: OutputStream = new OutputStream();
          fOut.setFilePath(data + '/cache_files/hello.txt.bz2');
          let cos: CompressorOutputStream = new CompressorStreamFactory(false).createCompressorOutputStream("bzip2", fOut);
          IOUtils.copy(inputStream, cos);
          cos.close();
          inputStream.close();
          done()
        })
    })

    it('testBzip2Unarchive', 0, function (done) {
      var context = featureAbility.getContext();
      context.getFilesDir()
        .then((data) => {
          let inputStream: InputStream = new InputStream();
          inputStream.setFilePath(data + '/cache_files/hello.txt.bz2');
          let fOut: OutputStream = new OutputStream();
          fOut.setFilePath(data + '/cache_files/hello.txt');
          let input: CompressorInputStream = new CompressorStreamFactory(false).createCompressorInputStream2("bzip2", inputStream);
          expect(input.read() == 49).assertTrue()
          expect(input.available() == 0).assertTrue()
          IOUtils.copy(input, fOut);
          inputStream.close();
          fOut.close();
          done()
        })
    })

    it('testSortFixture', 0, function (done) {
      let FIXTURE: Int8Array = new Int8Array([0, 1, 252, 253, 255, 254, 3, 2, 128])
      let ds: DS = setUpFixture(FIXTURE);
      ds.s.blockSort(ds.data, FIXTURE.length - 1);
      expect(ds.data.origPtr == 0).assertTrue()
      done()
    })
  })
}

class DS {
  public data: Data;
  public s: BlockSort;

  constructor(data: Data, s: BlockSort) {
    this.data = data;
    this.s = s;
  }
}

function setUpFixture(fixture: Int8Array): DS {
  let data: Data = new Data(1);
  System.arraycopy(fixture, 0, data.block, 1, fixture.length);
  return new DS(data, new BlockSort(data));
}

function setUpFixture2(FIXTURE2): DS {
  return setUpFixture(FIXTURE2);
}