/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import fileio from '@ohos.fileio';
import featureAbility from '@ohos.ability.featureAbility';
import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from "hypium/index";
import { InputStream, OutputStream, IOUtils, File, ArchiveOutputStream, ArchiveStreamFactory,
  TarArchiveInputStream, TarArchiveEntry, TarConstants, Long } from '@ohos/commons-compress';

export default function tarCompressorTest() {
  describe('tarCompressorTest', function () {
    it('tarCompressorOutputStreamTest', 0, function (done) {
      var context = featureAbility.getContext();
      context.getFilesDir()
        .then((data) => {
          var srcPath = data + "/tar"
          var src001 = srcPath + "/test1.xml"
          var tarDest001 = data + "/bla.tar"
          try {
            fileio.mkdirSync(srcPath);
          } catch (err) {
          }
          const writer = fileio.openSync(src001, 0o102, 0o666);
          fileio.writeSync(writer, "<?xml version = '1.0'?>\n"
          + "<!DOCTYPE connections>\n"
          + "<connections>\n"
          + "</connections>");
          fileio.closeSync(writer);
          testArArchiveCreation(data)
          done()
        })
    })

    it('tarCompressorInputStreamTest', 0, function (done) {
      var context = featureAbility.getContext();
      context.getFilesDir()
        .then((data) => {
          testUnCompressTar(data)
          done()
        })
    })
  })
}

function testArArchiveCreation(data) {
  let output: File = new File(data, 'bla.tar');
  let file1: File = new File(data + '/tar', 'test1.xml');
  let input1: InputStream = new InputStream();
  input1.setFilePath(file1.getPath());
  let out: OutputStream = new OutputStream();
  out.setFilePath(output.getPath());
  let os: ArchiveOutputStream = ArchiveStreamFactory.DEFAULT.createArchiveOutputStreamLittle("tar", out);
  let entry: TarArchiveEntry = new TarArchiveEntry();
  entry.tarArchiveEntryPreserveAbsolutePath2("testdata/test1.xml", false);
  entry.setModTime(Long.fromNumber(0));
  entry.setSize(Long.fromNumber(file1.length()));
  entry.setUserId(0);
  entry.setGroupId(0);
  entry.setUserName("avalon");
  entry.setGroupName("excalibur");
  entry.setMode(0o100000);
  os.putArchiveEntry(entry);
  IOUtils.copy(input1, os);
  os.closeArchiveEntry();
  os.close();
}

function testUnCompressTar(data: string) {
  let input: File = new File(data, 'bla.tar');
  let input1: InputStream = new InputStream();
  input1.setFilePath(input.getPath());
  let tais: TarArchiveInputStream = new TarArchiveInputStream(input1, TarConstants.DEFAULT_BLKSIZE,
    TarConstants.DEFAULT_RCDSIZE, null, false);
  let tarArchiveEntry: TarArchiveEntry = tais.getNextTarEntry();

  let name: string = tarArchiveEntry.getName();
  let tarFile: File = new File(data, name);
  if (name.indexOf('/') != -1) {
    try {
      let splitName: string = name.substring(0, name.lastIndexOf('/'));
      fileio.mkdirSync(data + '/' + splitName);
    } catch (err) {
    }
  }

  let fos: OutputStream = null;
  try {
    fos = new OutputStream();
    fos.setFilePath(tarFile.getPath())
    IOUtils.copy(tais, fos);
  } catch (e) {
    throw e;
  } finally {
    fos.close();
    input1.close();
    tais.close()
  }
}